package lesson.bisness;

/**
 * Created by IT school on 23.08.2017.
 */
public abstract class User implements Person{

    private String name;
    private int age;
    private Object id;

    public User(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public User(String name) { this.name = name; }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
