package lesson.bisness;

/**
 * Created by IT school on 23.08.2017.
 */
public class PremiumUser extends User{
    String billingInfo;

    public PremiumUser(String name, int age, String billingInfo) {
        super(name, age);
        this.billingInfo = billingInfo;
    }

    @Override
    void generateID() {
        id = System.currentTimeMillis();
    }

    @Override
    public String registration() {
        return "blablabla";
    }
}
